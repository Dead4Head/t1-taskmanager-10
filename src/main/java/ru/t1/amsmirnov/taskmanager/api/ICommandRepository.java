package ru.t1.amsmirnov.taskmanager.api;

import ru.t1.amsmirnov.taskmanager.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
