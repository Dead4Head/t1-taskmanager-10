package ru.t1.amsmirnov.taskmanager.api;

import ru.t1.amsmirnov.taskmanager.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    void clear();

}
