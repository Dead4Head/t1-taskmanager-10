package ru.t1.amsmirnov.taskmanager.api;

import ru.t1.amsmirnov.taskmanager.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
