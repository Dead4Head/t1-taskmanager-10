package ru.t1.amsmirnov.taskmanager.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
