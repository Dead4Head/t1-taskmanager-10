package ru.t1.amsmirnov.taskmanager.api;

import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    void clear();

}
