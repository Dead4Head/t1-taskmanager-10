package ru.t1.amsmirnov.taskmanager.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
