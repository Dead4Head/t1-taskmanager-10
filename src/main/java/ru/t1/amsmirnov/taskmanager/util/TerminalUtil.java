package ru.t1.amsmirnov.taskmanager.util;

import java.util.Scanner;

public interface TerminalUtil {

    final Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

}
