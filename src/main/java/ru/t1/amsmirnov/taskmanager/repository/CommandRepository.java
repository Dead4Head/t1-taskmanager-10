package ru.t1.amsmirnov.taskmanager.repository;

import ru.t1.amsmirnov.taskmanager.api.ICommandRepository;
import ru.t1.amsmirnov.taskmanager.constant.ArgumentConst;
import ru.t1.amsmirnov.taskmanager.constant.CommandConst;
import ru.t1.amsmirnov.taskmanager.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "Show command list.");
    private static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show developer info.");
    private static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show version info.");
    private static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system information.");
    private static final Command ARGUMENTS = new Command(CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show available arguments.");
    private static final Command COMMANDS = new Command(CommandConst.COMMANDS, ArgumentConst.COMMANDS, "Show available commands.");
    private static final Command PROJECT_LIST = new Command(CommandConst.PROJECT_LIST, null, "Show project list.");
    private static final Command PROJECT_CREATE = new Command(CommandConst.PROJECT_CREATE, null, "Create new project.");
    private static final Command PROJECT_CLEAR = new Command(CommandConst.PROJECT_CLEAR, null, "Clear project list.");
    private static final Command TASK_LIST = new Command(CommandConst.TASK_LIST, null, "Show task list.");
    private static final Command TASK_CREATE = new Command(CommandConst.TASK_CREATE, null, "Create new task.");
    private static final Command TASK_CLEAR = new Command(CommandConst.TASK_CLEAR, null, "Clear task list.");
    private static final Command EXIT = new Command(CommandConst.EXIT, null, "Close Task Manager.");

    private static final Command[] TERMINAL_COMMANDS = new Command[] {HELP, ABOUT, VERSION, INFO, ARGUMENTS, COMMANDS,
            PROJECT_LIST,PROJECT_CREATE,PROJECT_CLEAR,
            TASK_LIST,TASK_CREATE,TASK_CLEAR,
            EXIT};

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
