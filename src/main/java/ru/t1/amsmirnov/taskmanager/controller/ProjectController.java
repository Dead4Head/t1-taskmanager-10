package ru.t1.amsmirnov.taskmanager.controller;

import ru.t1.amsmirnov.taskmanager.api.IProjectService;
import ru.t1.amsmirnov.taskmanager.api.IProjectController;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final  IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER PROJECT NAME:");
        final String projectName = TerminalUtil.SCANNER.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String projectDescription = TerminalUtil.SCANNER.nextLine();
        final Project project = projectService.create(projectName, projectDescription);
        if (project == null) System.err.println("ERROR");
        else System.out.println("[OK]");
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECT LIST]");
        int index = 1;
        final List<Project> projects = projectService.findAll();
        for (final Project project: projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS LIST]");
        projectService.clear();
        System.out.println("[OK]");
    }

}
