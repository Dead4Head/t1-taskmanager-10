package ru.t1.amsmirnov.taskmanager.controller;

import ru.t1.amsmirnov.taskmanager.api.ITaskController;
import ru.t1.amsmirnov.taskmanager.api.ITaskService;
import ru.t1.amsmirnov.taskmanager.model.Task;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME:");
        final String taskName = TerminalUtil.SCANNER.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String taskDescription = TerminalUtil.SCANNER.nextLine();
        final Task task = taskService.createTask(taskName, taskDescription);
        if (task == null) System.err.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        int index = 1;
        final List<Task> taskList = taskService.findAll();
        for (final Task task: taskList) {
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASK LIST]");
        taskService.clear();
        System.out.println("[OK]");
    }

}
